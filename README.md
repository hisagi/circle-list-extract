# Circle list extract

コミティアのwebページからサークルリストを抽出するツール

## これはなに

コミティアのwebページからサークルリストを取得し、csvで出力するツールです。
コミティア115以降に対応しています。

## How to use

`python main.py [開催回]`

### 動作確認環境

- Ubuntu 18.04.3 LTS 64bit
- Python 3.6.8


### 必要なパッケージ

必要なパッケージはrements.txtに記述しています。  
`pip install -r requirements.txt` で一括インストール可能です。

## LICENSE

WTFPL
