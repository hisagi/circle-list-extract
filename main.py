#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import re
import jaconv
import csv
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

def main():
  args = sys.argv
  web_url = 'http://www.comitia.co.jp/history/' + args[1]  + 'list_sp.html'
  out_file_csv = args[1] + '.csv'
  
  print("Target URL: " + web_url)
  print("Output name:" + out_file_csv)

  html = urlopen(web_url)
  bsObj = BeautifulSoup(html, "html.parser")

  table = bsObj.findAll("table")[0]
  rows = table.findAll("tr")

  with open(out_file_csv, "w", encoding='utf-8') as file:
    writer = csv.writer(file)
    writer.writerow({'space','circle name'})
    for row in rows:
      csvRow = []
      for cell in row.findAll(['td', 'th']):
        csvRow.append(jaconv.z2h(cell.get_text(), kana=False, digit=True, ascii=True))
      writer.writerow(csvRow)

  # 本当はcsv.writeの時点でいい感じに排除したかったけどできなかったので・・・
  deleteIndex(out_file_csv)

  print("Done.")

def deleteIndex(file_name):
  f = open(file_name)
  output = []
  for line in f:
    if not re.compile('^(.|展示)$').search(line):
      output.append(line)
  f.close()
  f = open(file_name, "w")
  f.writelines(output)
  f.close()


if __name__ == "__main__":
  main()

